package sets

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestStrings(t *testing.T) {
	s := NewStrings()
	s.Add("A")
	s.Add("B")

	require.Equal(t, 2, s.Cardinality())
	require.True(t, s.Contains("B"))
	require.False(t, s.Contains("C"))

	s2 := NewStrings()
	s2.Add("B")
	s2.Add("C")

	s3 := s.Intersect(s2)
	require.Equal(t, 1, s3.Cardinality())
	require.True(t, s3.Contains("B"))
	require.False(t, s3.Contains("A"))
	require.False(t, s3.Contains("C"))

	s1Slice := s.Slice()
	require.ElementsMatch(t, s1Slice, []string{"A", "B"})
}
