// Common is a collection of common packages that are shared between projects.
//
// This allows for cleaner code usage within Eckler and also reduces dependency
// bloat when using external dependencies.
package common
