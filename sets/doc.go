//Sets is a package of set data structures that are strongly typed so it is easy
//to use in go without the issue of casting from interface{}
package sets
