# common

[![pipeline status](https://gitlab.fs.eckler.ca/life-2/common/badges/master/pipeline.svg)](https://gitlab.fs.eckler.ca/life-2/common/commits/master)
[![coverage report](https://gitlab.fs.eckler.ca/life-2/common/badges/master/coverage.svg)](https://gitlab.fs.eckler.ca/life-2/common/commits/master)

This is a utility package with many sub packages of common go functionalities shared across projects.