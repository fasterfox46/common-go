package sets

//Strings represents a set of strings
type Strings struct {
	data map[string]bool
}

//NewStrings initializes a new set of strings
func NewStrings(initialValues ...string) *Strings {
	data := make(map[string]bool)
	for _, v := range initialValues {
		data[v] = true
	}
	return &Strings{
		data: data,
	}
}

//Add will add one or more values to the set
func (ss *Strings) Add(values ...string) {
	for _, v := range values {
		ss.data[v] = true
	}
}

//Cardinality will return the number of unique elements in the set
func (ss *Strings) Cardinality() int {
	return len(ss.data)
}

//Contains will determine if the string provided exists in the set
func (ss *Strings) Contains(value string) bool {
	v, ok := ss.data[value]
	return ok && v
}

//Intersect will return a new set with only the values found in both sets
func (ss *Strings) Intersect(ss2 *Strings) *Strings {
	src, dest := ss, ss2
	if len(ss.data) > len(ss2.data) {
		src, dest = ss2, ss
	}
	results := make(map[string]bool)
	for v := range src.data {
		if dest.Contains(v) {
			results[v] = true
		}
	}
	return &Strings{
		data: results,
	}
}

//Creates a slice of the values in the set, the values are not sorted
func (ss *Strings) Slice() []string {
	results := make([]string, 0, len(ss.data))
	for v := range ss.data {
		results = append(results, v)
	}
	return results
}
